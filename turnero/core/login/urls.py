from django.contrib import admin
from django.urls import include,path
from turnero import settings
from .models import *
from .views import *
from django.conf.urls.static import static

from django.contrib.auth.views import LoginView, LogoutView

app_name = 'login'
urlpatterns = [
    path('account/', LoginFormView.as_view(), name='loginView'),
    path('account2/', custom_list, name='loginView2'),
]