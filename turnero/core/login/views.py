from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import redirect


# Create your views here.

def custom_list(request):
    return render(request, 'templates:principal.html')


class LoginFormView(LoginView):
    template_name = 'login.html'
    redirect_authenticated_user = True;
    print("Template name %s" % template_name)

    # def dispatch(self, request, *args, **kwargs):
    #     print("User = %s" %request.user)
    #     print("items = %s" %request.GET.urlencode)
    #     if request.user.is_authenticated:
    #         return redirect('login:account2')
    #     return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Iniciar Sesion'
        # context['next'] = None
        return context
