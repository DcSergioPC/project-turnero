from django.apps import AppConfig


class TurneroAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'core.turnero_app'
