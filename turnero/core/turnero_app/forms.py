from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import gettext as _

from .models import *


class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        # fields = ["id", "persona", "area", "nombre_usuario", "contrasena"]
        fields = '__all__'
        # widgets = {
        #     'contrasena': forms.PasswordInput(render_value=True,attrs={'class':'bg-dark border-dark'}),
        #     'persona': forms.Select(attrs={'class':'bg-dark border-dark'}),
        #     'area':forms.TextInput(attrs={'class':'bg-dark border-dark'}),
        #     'nombre_usuario':forms.TextInput(attrs={'class':'bg-dark border-dark'}),
        # }


class PersonaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class']='bg-dark border-dark form-control'
            form.field.widget.attrs['autocomplete']='off'

    class Meta:
        model = Persona
        # fields = ["id","tipo_documento","nro_documento","nombre","fecha_nacimiento","sexo"]
        fields = '__all__'


class ClienteForm(forms.ModelForm):
    class Meta:
        model= Cliente
        fields = '__all__'


class ColaListaForm(forms.ModelForm):
    class Meta:
        model= ColaLista
        fields = '__all__'


class NroDocumentoForm(forms.Form):
    nro = forms.CharField(validators=[RegexValidator(r'^[0-9]{6,9}(-[0-9])?\Z', 'Numero de documento incorrecto'), ])

    def clean_nro(self):
        nro = self.cleaned_data['nro']
        if not (Persona.objects.filter(nro_documento=nro).exists() and Cliente.objects.filter(persona=Persona.objects.get(nro_documento=nro)).exists()):
            raise ValidationError(_("El documento no pertenece a un cliente"))
        return nro
        try:
            cli = Cliente.objects.get(
                persona=Persona.objects.get(nro_documento=nro))
            # Aqui se va encargar de enviar la cedula a donde le interese a la pagina-----------------------------------
        except ValidationError as ex:
            raise ValidationError(_("El documento no pertenece a un cliente"))
        return nro

