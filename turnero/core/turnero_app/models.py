# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
import datetime
from django.db import models

from django.contrib import admin

from .choices import SEXO, PRIORIDAD


# class Cargo(models.Model):
#     cargo_id = models.AutoField(primary_key=True)
#     estado_cargo = models.BooleanField()
#     cargo_descripcion = models.CharField(max_length=25)
#     fecha_insercion = models.DateTimeField(null=True)
#     usuario_modificacion = models.CharField(max_length=16,blank=True,null=True)
#     fecha_modificacion = models.DateTimeField(null=True)
#     usuario_insercion = models.CharField(max_length=16,blank=True,null=True)
#     email = models.EmailField(max_length=254,null=True)

#     def __str__ (self):
#         return self.cargo_descripcion

#     class Meta:
#         db_table = "cargo"


def prioridadNivel(prio):
    if prio.id == 1:
        return "Alta"
    elif prio.id == 2:
        return "Media"
    return "Baja"


def documentoChoices():
    i = 1
    l = []

    for obj in TipoDeDocumento.objects.all():
        k = (i, obj.descripcion)
        l.append(k)
        i = i + 1
    return tuple(l)


class TipoDeDocumento(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=64)

    def __str__(self) -> str:
        return self.descripcion
        return str(self.id)

    class Meta:
        db_table = 'Tipo de Documento'


class Persona(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_documento = models.ForeignKey('TipoDeDocumento', on_delete=models.CASCADE)
    nro_documento = models.CharField(unique=True, max_length=16)
    nombre = models.CharField(max_length=64)
    apellido = models.CharField(max_length=64, blank=True, null=True)
    fecha_nacimiento = models.DateField()
    telefono = models.CharField(max_length=32, blank=True, null=True)
    direccion = models.CharField(max_length=128, blank=True, null=True)
    email = models.EmailField(max_length=64, blank=True, null=True)
    # sexo = models.BooleanField(blank=True, null=True)
    sexo = models.CharField(max_length=1, choices=SEXO, default='M')

    def getNombre(self):
        return self.nombre

    def getApellido(self):
        return self.apellido

    def __str__(self):
        return "{} {} {}".format(self.id,self.nombre,self.apellido)
        # return str(self.id) + " " + self.nombre + " " + self.apellido
        # return str(self.id)

    class Meta:
        db_table = 'Persona'


class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    fecha_de_registro = models.DateField(default=datetime.date.today)
    ultima_conexion = models.DateTimeField(blank=True, null=True)
    area = models.CharField(max_length=64, blank=True, null=True)
    nombre_usuario = models.CharField(max_length=64)
    contrasena = models.CharField(max_length=64)

    def __str__(self) -> str:
        return self.nombre_usuario
        return str(self.id)

    class Meta:
        db_table = 'Usuario'
        unique_together = (('persona', 'nombre_usuario'),)


class Servicio(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=128)

    def getDescripcion(self):
        return self.descripcion

    def __str__(self) -> str:
        return str(self.id)

    class Meta:
        db_table = 'Servicio'


class Box(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=128)
    servicio = models.ForeignKey('Servicio', on_delete=models.CASCADE)
    estado = models.BooleanField()

    def getDescripcion(self):
        return self.descripcion

    def getServicio(self):
        return self.servicio

    def __str__(self) -> str:
        return str(self.id)

    class Meta:
        verbose_name = "Box"
        verbose_name_plural = "Boxes"
        db_table = 'Box'


class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    persona = models.OneToOneField('Persona', on_delete=models.CASCADE)

    @admin.display(description='Prioridad')
    def prioridad(self):
        return prioridadNivel(ColaLista.objects.get(cliente=self.id).prioridad)
        pri = (ColaLista.objects.get(cliente=self.id))
        return "Alta" if pri == 1 else "Media" if pri == 2 else "Baja"

    @admin.display(description='En espera',boolean=True)
    def en_espera(self) -> bool:
        return (ColaLista.objects.get(cliente=self.id).en_espera)

    def __str__(self) -> str:
        return str(self.persona)

    class Meta:
        db_table = 'Cliente'


class Cola(models.Model):
    id = models.AutoField(primary_key=True)
    box = models.OneToOneField(Box, on_delete=models.CASCADE)

    @admin.display(description='Box')
    def boxDescripcion(self):
        return "%s" % (self.box.getDescripcion())

    @admin.display(description='Servicio')
    def boxServicioDescripcion(self):
        return "%s" % (self.box.getServicio().getDescripcion())

    def __str__(self) -> str:
        return self.box.servicio.descripcion + " " + self.box.descripcion

    class Meta:
        db_table = 'Cola'


class Prioridad(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=128)

    def __str__(self) -> str:
        return self.descripcion

    def getprioridad(self):
        return prioridadNivel(self)

    class Meta:
        verbose_name = "Prioridad"
        verbose_name_plural = "Prioridades"
        db_table = 'Prioridad'


def defaultPriority(client):
    # fecha_nacimiento = Persona.objects.get(pk=cliente.persona).fecha_nacimiento
    fecha_nacimiento = Persona.objects.get(pk=client.persona).fecha_nacimiento
    print(fecha_nacimiento)
    d1 = datetime.date.today()
    d2 = fecha_nacimiento
    if int(abs((d1 - d2).days) / 365) >= 65:
        return 1
    else:
        return 3


class ColaLista(models.Model):


    id = models.AutoField(primary_key=True)
    cliente = models.OneToOneField(Cliente, on_delete=models.CASCADE)


    PRIO = (
        (Prioridad.objects.get(pk=1),'ALTA'),
        (Prioridad.objects.get(pk=2),'MEDIA'),
        (Prioridad.objects.get(pk=3),'BAJA'),
    )
    PRIO2 = (
        (Prioridad.objects.get(pk=1).pk,'ALTA'),
        (Prioridad.objects.get(pk=2).pk,'MEDIA'),
        (Prioridad.objects.get(pk=3).pk,'BAJA'),
    )

    cola = models.ForeignKey(Cola, on_delete=models.CASCADE)
    prioridad = models.ForeignKey('Prioridad', default=3,
                                  on_delete=models.CASCADE)
    usuario = models.ForeignKey('Usuario', on_delete=models.CASCADE, null=True, blank=True)
    en_espera = models.BooleanField(default=True)
    hora = models.TimeField(default=datetime.datetime.now().strftime("%H:%M:%S"))

    @admin.display(description='Nombre y Apellido')
    def nombreCompleto(self):
        return "%s %s" % (self.cliente.persona.getNombre(), self.cliente.persona.getApellido())

    @admin.display(description='Box')
    def box(self):
        return "%s" % (self.cola.box.getDescripcion())

    @admin.display(description='Servicio')
    def servicio(self):
        return "%s" % (self.cola.box.servicio.getDescripcion())

    @admin.display(description='Cola')
    def colaid(self):
        return "%s" % (self.cola.id)

    @admin.display(description='Prioridad')
    def prioridadNivel(self):
        return prioridadNivel(self.prioridad)

    def __str__(self) -> str:
        return str(self.id)

    class Meta:
        db_table = 'Cola Lista'
