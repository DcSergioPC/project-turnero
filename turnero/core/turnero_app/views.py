import json

import django.forms
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.db import models
from django.http import JsonResponse
from django.shortcuts import render

from django.shortcuts import redirect
from django.views.generic.edit import View, CreateView
from django.views.decorators.csrf import csrf_protect
# Create your views here.

from core.turnero_app.models import *
from core.turnero_app.forms import *

from django.views.generic import ListView, TemplateView, FormView
from django.forms import models

from .forms import *
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib import messages

from django.utils.translation import gettext as _

from django.forms import formset_factory


class PersonaListView(ListView):
    template_name = 'persona_list.html'
    # context_object_name='persona_list'
    context_object_name = 'persona_list'


class ClienteFormView(FormView):
    template_name = 'principal.html'
    form_class = NroDocumentoForm
    success_url = reverse_lazy("prueba_link")

    def form_valid(self, form):
        try:
            cli = Cliente.objects.get(persona=Persona.objects.get(nro_documento=form.data.get('nro')))
        except Exception as ex:
            form.add_error('nro', ValidationError(_("El documento no pertenece a un ciente")))
            return self.form_invalid(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

# @login_required
class VerColaView(View):
    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     # context["qs_json"] = json.dumps(list(Cliente.objects.values()))
    #     return context
    class Counter:
        count = 0

        def increment(self):
            self.count += 1
            return self.count

        def cero(self):
            self.count = 0
            return ''

    context = {'servicio_list': Servicio.objects.all(),
               'box': Box.objects.all(),
               'colalista': ColaLista.objects.all().order_by('-en_espera', 'prioridad_id', 'hora'),
               'counter': Counter(),
               'show': False
               }

    def reloadcontext(self):
        self.context['servicio_list'] = Servicio.objects.all()
        self.context['box'] = Box.objects.all()
        self.context['colalista'] = ColaLista.objects.all().order_by('-en_espera', 'prioridad_id', 'hora')
        self.context['counter'] = self.Counter()
        self.context['show'] = False

    def get(self, request):
        # code for GET request...
        # if request.GET == {}:
        #     formClient = NroDocumentoForm(prefix="client_form")
        #     formReg = PersonaForm(prefix="registro_form")
        #     context = {
        #         'client_form': formClient,
        #         'registro_form': formReg,
        # 'qs_json': json .dumps(list(Cliente.objects.values()))
        # }

        # context = {'servicio_list': Servicio.objects.all(),
        #            'box': Box.objects.all(),
        #            'colalista': ColaLista.objects.all().order_by('-en_espera', 'prioridad_id', 'hora'),
        #            }
        self.reloadcontext()
        self.context['show'] = False
        return render(request, "cola.html", self.context)
        # return redirect("turno_name")
        # return render(request, "cliente.html")
        # form = NroDocumentoForm()
        # form.full_clean()
        # return render(request,"cliente.html",{"client_form":form})

    def post(self, request):
        # instantiate all unique forms (using prefix) as unbound

        # determine which form is submitting (based on hidden input called 'action')
        # action = self.request.POST['action']
        # context = self.get_context_data()
        # bind to POST and process the correct form
        if 'delete' in request.POST:
            colalista_id = request.POST['delete']
            self.reloadcontext()
            if not ColaLista.objects.filter(pk=colalista_id).exists():
                messages.error(request, _('El cliente ya estaba fuera de la cola'))
                return render(request, 'cola.html', self.context)
            colalista = ColaLista.objects.get(pk=colalista_id)
            show = colalista.cola.box
            self.context['show'] = show
            colalista.delete()
            messages.success(request, _('Se ha removido de la cola satisfactoriamente'))
            # return redirect('vercola_name')
            return render(request, 'cola.html', self.context)
            # if (not 'prioridad' in request.POST) or (not 'boxradio' in request.POST):
            #     if not 'prioridad' in request.POST:
            #         self.context['errorprioridad'] = _("No has seleccionado ninguna prioridad")
            #     if not 'boxradio' in request.POST:
            #         self.context['errorboxradio'] = _("No has seleccionado ningun box")
            #     return render(request, "cliente.html", self.context)
            # boxradio = request.POST['boxradio']
            # prioridad = request.POST['prioridad']
            # box = Box.objects.get(id=boxradio)

            return redirect('principal_name')
        return render(request, 'cola.html')
class SoloVerColaView(View):
    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     # context["qs_json"] = json.dumps(list(Cliente.objects.values()))
    #     return context
    class Counter:
        count = 0

        def get(self):
            return self.count

        def increment(self):
            self.count += 1
            return self.count

        def cero(self):
            self.count = 0
            return ''

    context = {'servicio_list': Servicio.objects.all(),
               'box': Box.objects.all(),
               'colalista': ColaLista.objects.all().order_by('-en_espera', 'prioridad_id', 'hora'),
               'counter': Counter(),
               }

    def reloadcontext(self):
        self.context['servicio_list'] = Servicio.objects.all()
        self.context['box'] = Box.objects.all()
        self.context['colalista'] = ColaLista.objects.all().order_by('-en_espera', 'prioridad_id', 'hora')
        self.context['counter'] = self.Counter()

    def get(self, request):
        # code for GET request...
        # if request.GET == {}:
        #     formClient = NroDocumentoForm(prefix="client_form")
        #     formReg = PersonaForm(prefix="registro_form")
        #     context = {
        #         'client_form': formClient,
        #         'registro_form': formReg,
        # 'qs_json': json .dumps(list(Cliente.objects.values()))
        # }

        # context = {'servicio_list': Servicio.objects.all(),
        #            'box': Box.objects.all(),
        #            'colalista': ColaLista.objects.all().order_by('-en_espera', 'prioridad_id', 'hora'),
        #            }
        self.reloadcontext()
        self.context['show'] = False
        return render(request, "solovercola.html", self.context)
        # return redirect("turno_name")
        # return render(request, "cliente.html")
        # form = NroDocumentoForm()
        # form.full_clean()
        # return render(request,"cliente.html",{"client_form":form})

    def post(self, request):

        return render(request, 'cola.html')


def home_view(request):
    FormBase = PersonaForm
    context = {}
    # formset = formset_factory(UsuarioForm)
    if request.method == 'GET':
        form = FormBase()
        return render(request, "principal.html", {"form": form})
    # create object of form
    form = FormBase(request.POST)
    # formset = formset(request.POST)
    # check if form data is valid
    # if formset.is_valid():
    if form.is_valid():
        # save the form data to model
        form.save(commit=False)
        # formset.save(commit=False)

    context['form'] = form
    # context['form'] = formset
    return render(request, "principal.html", context)


# @login_required
class PrincipalView(CreateView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["qs_json"] = json.dumps(list(Cliente.objects.values()))
        return context

    def get(self, request):
        # code for GET request...
        if request.GET == {}:
            formClient = NroDocumentoForm(prefix="client_form")
            formReg = PersonaForm(prefix="registro_form")
            context = {
                'client_form': formClient,
                'registro_form': formReg,
            }
            return render(request, "principal.html", context)
        return render(request, "principal.html")
        # form = NroDocumentoForm()
        # form.full_clean()
        # return render(request,"principal.html",{"client_form":form})

    def post(self, request):
        context = {}
        # instantiate all unique forms (using prefix) as unbound
        client_form = NroDocumentoForm(prefix='client_form')
        reg_form = PersonaForm(prefix='registro_form')

        # determine which form is submitting (based on hidden input called 'action')
        action = self.request.POST['action']

        # bind to POST and process the correct form
        if (action == 'client_nro'):
            client_form = NroDocumentoForm(self.request.POST, prefix='client_form')
            if client_form.is_valid():
                # user form validated, code away..
                # try:
                cli = Cliente.objects.get(
                    persona=Persona.objects.get(nro_documento=client_form.cleaned_data['nro']))
                #     # Aqui se va encargar de enviar la cedula a donde le interese a la pagina-----------------------------------
                context = {'servicio_list': Servicio.objects.all(),
                           'box': Box.objects.all(),
                           'cliente': cli}
                # request.session['servicio_list'] = Servicio.objects.all()
                # request.session['box'] = Box.objects.all()
                request.session['cliente'] = cli.id
                encola = ColaLista.objects.filter(cliente=cli)
                client_form = NroDocumentoForm(prefix='client_form')
                if encola.exists():
                    encola = ColaLista.objects.get(cliente=cli)
                    messages.error(request,
                                   _("El cliente {} ya se encuentra en la cola en el {} del {}".format(cli.persona,
                                                                                                       encola.cola.box.descripcion,
                                                                                                       encola.cola.box.servicio.descripcion)))
                else:
                    return redirect('turno_name')
                # return render(request, 'cliente.html',context)
            #     return render(request,'cliente.html', {'client': cli})
            # except ValidationError as ex:
            #     client_form.add_error('nro', ValidationError(_("El documento no pertenece a un cliente")))
        elif action == 'registro_form':
            reg_form = PersonaForm(self.request.POST, prefix='registro_form')
            if reg_form.is_valid():
                pers = reg_form.save()
                c = Cliente.objects.create(persona=pers)
                client_form = NroDocumentoForm(initial={'nro': reg_form.cleaned_data['nro_documento']},
                                               prefix='client_form')
                client_form.is_valid()
                reg_form = PersonaForm(prefix='registro_form')
                messages.success(self.request, _('Registro Exitoso'))

        # prep context
        # context = {
        #     'client_form': client_form,
        #     'registro_form': reg_form,
        # }
        context['client_form'] = client_form
        context['registro_form'] = reg_form
        return render(request, 'principal.html', context)


# @login_required
class ServicioView(CreateView):

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     # context["qs_json"] = json.dumps(list(Cliente.objects.values()))
    #     return context

    def get(self, request):
        # code for GET request...
        # if request.GET == {}:
        #     formClient = NroDocumentoForm(prefix="client_form")
        #     formReg = PersonaForm(prefix="registro_form")
        #     context = {
        #         'client_form': formClient,
        #         'registro_form': formReg,
        # 'qs_json': json .dumps(list(Cliente.objects.values()))
        # }

        context = {'servicio_list': Servicio.objects.all(),
                   'box': Box.objects.all(),
                   'cliente': Cliente.objects.get(id=request.session['cliente']),
                   'edad': datetime.date.today().year - Cliente.objects.get(
                       id=request.session['cliente']).persona.fecha_nacimiento.year,
                   'prioridad_list': Prioridad.objects.all()}
        return render(request, "cliente.html", context)
        # return redirect("turno_name")
        # return render(request, "cliente.html")
        # form = NroDocumentoForm()
        # form.full_clean()
        # return render(request,"cliente.html",{"client_form":form})

    def post(self, request):
        # instantiate all unique forms (using prefix) as unbound

        # determine which form is submitting (based on hidden input called 'action')
        action = self.request.POST['action']

        # context = self.get_context_data()
        context = {'servicio_list': Servicio.objects.all(),
                   'box': Box.objects.all(),
                   'cliente': Cliente.objects.get(id=request.session['cliente']),
                   'edad': datetime.date.today().year - Cliente.objects.get(
                       id=request.session['cliente']).persona.fecha_nacimiento.year,
                   'prioridad_list': Prioridad.objects.all()}
        # bind to POST and process the correct form
        if action == 'save':
            if (not 'prioridad' in request.POST) or (not 'boxradio' in request.POST):
                if not 'prioridad' in request.POST:
                    context['errorprioridad'] = _("No has seleccionado ninguna prioridad")
                if not 'boxradio' in request.POST:
                    context['errorboxradio'] = _("No has seleccionado ningun box")
                return render(request, "cliente.html", context)
            boxradio = request.POST['boxradio']
            prioridad = request.POST['prioridad']
            box = Box.objects.get(id=boxradio)
            client = Cliente.objects.get(pk=request.session['cliente'])
            if Cola.objects.get_or_create(box=box):
                if ColaLista.objects.get_or_create(cola=Cola.objects.get(box=box),
                                                   prioridad_id=prioridad, en_espera=True,
                                                   hora=datetime.datetime.now().strftime("%H:%M:%S"),
                                                   cliente_id=request.session['cliente']):
                    client = Cliente.objects.get(pk=request.session['cliente'])
                    del request.session['cliente']
                    # request.session.modified = True
                    messages.success(request,
                                     _("Se ha puesto en el {} del {} a {} satisfactoriamente".format(box.descripcion,
                                                                                                     box.servicio.descripcion,
                                                                                                     client.persona)))
                else:
                    messages.error(request,
                                   _("No se ha podido colocar en el {} del {} a {} satisfactoriamente".format(
                                       box.descripcion,
                                       box.servicio.descripcion,
                                       client.persona)))
            else:
                messages.error(request,
                               _("No se ha podido colocar en el {} del {} a {} satisfactoriamente".format(
                                   box.descripcion,
                                   box.servicio.descripcion,
                                   client.persona)))
            if 'cliente' in request.session:
                del request.session['cliente']
            return redirect('principal_name')
        if action == 'cancel':
            if 'cliente' in request.session:
                del request.session['cliente']
            return redirect('principal_name')
