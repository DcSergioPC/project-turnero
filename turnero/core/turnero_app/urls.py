from django.contrib import admin
from django.urls import include, path
from turnero import settings
from .models import *
from .views import *
from django.conf.urls.static import static

from django.contrib.auth.views import LoginView, LogoutView

app_name = 'app_turnero'
urlpatterns = [
    path('principaltur/', PrincipalView.as_view(), name='Principal'),
]
