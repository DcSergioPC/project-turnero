from django.contrib import admin
from .forms import *
from django.contrib.auth.models import Group, User
from django.contrib.auth.admin import GroupAdmin, UserAdmin

# Register your models here.


class TipoDeDocumentoAdmin(admin.ModelAdmin):
    list_display = ["id", "descripcion"]
    ordering = ["id"]


class PersonaAdmin(admin.ModelAdmin):
    list_display = ["id", "nombre", "apellido", "nro_documento",
                    "sexo", "fecha_nacimiento", "telefono", "direccion", "email"]
    search_fields = ["nombre", "apellido", "nro_documento"]
    form = PersonaForm


class UsuarioAdmin(admin.ModelAdmin):
    list_display = ["id", "nombre_usuario", "area",
                    "fecha_de_registro", "ultima_conexion"]
    form = UsuarioForm


class ServicioAdmin(admin.ModelAdmin):
    list_display = ["id", "descripcion"]
    ordering = ["id"]


@admin.display(description='Servicio')
def servicioDescripcion(obj):
    return "%s" % (obj.servicio.getDescripcion())


class BoxAdmin(admin.ModelAdmin):

    list_display = ["id", "descripcion", servicioDescripcion, "estado"]


class PrioridadAdmin(admin.ModelAdmin):
    list_display = ["id", "descripcion"]
    ordering = ["id"]


@admin.display(description='Nombre y Apellido')
def clientePersona(obj):
    return "%s %s" % (obj.persona.getNombre(), obj.persona.getApellido())


class ClienteAdmin(admin.ModelAdmin):
    list_display = ["id", clientePersona, 'prioridad', 'en_espera']
    form = ClienteForm
    pass


class ColaAdmin(admin.ModelAdmin):
    list_display = ["id", 'boxDescripcion', 'boxServicioDescripcion']


class ColaListaAdmin(admin.ModelAdmin):
    list_display = ["id", 'nombreCompleto', 'servicio', 'box',
                    'colaid', 'prioridadNivel', 'en_espera', 'hora']
    pass


admin.site.register(TipoDeDocumento, TipoDeDocumentoAdmin)
admin.site.register(Persona, PersonaAdmin)
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Servicio, ServicioAdmin)
admin.site.register(Prioridad, PrioridadAdmin)
admin.site.register(Box, BoxAdmin)
admin.site.register(Cola, ColaAdmin)
admin.site.register(ColaLista, ColaListaAdmin)
