"""turnero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import include, path
from pip._internal.utils.urls import url_to_path

from . import settings

from django.urls import reverse_lazy
from core.turnero_app.models import Persona
from core.turnero_app.views import *
from django.conf.urls.static import static
from core.login.views import LoginFormView
from django.views.generic import TemplateView, RedirectView

from django.contrib.auth.decorators import login_required

from django.contrib.auth.views import LoginView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('principal/', login_required(PrincipalView.as_view()), name='principal_name'),
    # path('^$', redirect('principal_name')),
    path('', RedirectView.as_view(url=reverse_lazy('principal_name'))),
    # path('principalanterior/', ClienteFormView.as_view(), name='princi_name'),
    # path('principalprueba/', home_view, name='principalea_name'),
    path('principal/vercola/', login_required(VerColaView.as_view()), name='vercola_name'),
    path('principal/cola/', login_required(SoloVerColaView.as_view()), name='solovercola_name'),
    path('login/', LoginFormView.as_view(), name='login'),
    # path('principal/registro', TemplateView.as_view(template_name='body.html'), name='prueba_link'),
    path('principal/turno/', login_required(ServicioView.as_view()), name='turno_name'),
    # path('__debug__/', include('debug_toolbar.urls')),

]
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
